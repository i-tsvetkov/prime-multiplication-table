#!/usr/bin/env ruby
require 'prime'
require_relative 'src/table_printer'

size = begin
         input = ARGV.first
         Integer(input)
       rescue
         warn 'Please specify the size of the table as the first argument.'
         warn "The provided '#{input}' is not valid."

         exit(1)
       end

primes = Prime.take(size)

multiplication_table = primes.map do |prime1|
  primes.map do |prime2|
    prime1 * prime2
  end
end

printer = TablePrinter.new(
  rows: multiplication_table,
  row_labels: primes,
  column_labels: primes
)

printer.print

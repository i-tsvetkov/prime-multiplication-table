require_relative 'test_helper'
require_relative '../src/table_printer'

describe TablePrinter do
  before do
    @rows = [
      [1, 2, 3],
      [4, 5, 6],
    ]

    @row_labels = [:x, Time.at(1234567890).utc]
    @column_labels = ['a', :b, 'c']

    @dir = Pathname(__dir__).join('data', 'table_printer')
  end

  describe :print do
    before do
      @io = StringIO.new
    end

    {
      unicode: TablePrinter::UNICODE_BOX_CHARACTERS,
      ascii:   TablePrinter::ASCII_BOX_CHARACTERS,
    }.each do |style, drawing_characters|

      describe style do

        describe 'with row labels' do
          before do
            @printer = TablePrinter.new(
              rows: @rows,
              row_labels: @row_labels,
              drawing_characters: drawing_characters,
              io: @io
            )
          end

          it do
            @printer.print

            _(@io.string).must_equal File.read(@dir.join("row_labels_#{style}.txt"))
          end
        end

        describe 'with column labels' do
          before do
            @printer = TablePrinter.new(
              rows: @rows,
              column_labels: @column_labels,
              drawing_characters: drawing_characters,
              io: @io
            )
          end

          it do
            @printer.print

            _(@io.string).must_equal File.read(@dir.join("column_labels_#{style}.txt"))
          end
        end

        describe 'with row and column labels' do
          before do
            @printer = TablePrinter.new(
              rows: @rows,
              row_labels: @row_labels,
              column_labels: @column_labels,
              drawing_characters: drawing_characters,
              io: @io
            )
          end

          it do
            @printer.print

            _(@io.string).must_equal File.read(@dir.join("row_column_labels_#{style}.txt"))
          end
        end

        describe 'without labels' do
          before do
            @printer = TablePrinter.new(
              rows: @rows,
              drawing_characters: drawing_characters,
              io: @io
            )
          end

          it do
            @printer.print

            _(@io.string).must_equal File.read(@dir.join("without_labels_#{style}.txt"))
          end
        end

      end

    end
  end
end

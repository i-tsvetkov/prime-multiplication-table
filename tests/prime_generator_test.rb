require 'prime'
require_relative 'test_helper'
require_relative '../src/prime_generator'

describe PrimeGenerator do
  before do
    @primes = PrimeGenerator.new
  end

  describe :take do
    it 'generates the prime numbers correctly' do
      _(@primes.take(1000)).must_equal Prime.take(1000)
    end
  end
end

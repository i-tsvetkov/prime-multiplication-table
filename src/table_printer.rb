class TablePrinter
  ASCII_BOX_CHARACTERS = {
    header_beginning: '+',
    header_crossing: '+',
    header_end: '+',

    row_beginning: '|',
    row_crossing: '|',
    row_end: '|',

    row_separator_beginning: '+',
    row_separator_crossing: '+',
    row_separator_end: '+',

    footer_beginning: '+',
    footer_crossing: '+',
    footer_end: '+',

    horizontal_line: '-',
  }.freeze

  UNICODE_BOX_CHARACTERS = {
    header_beginning: "\u250c",
    header_crossing: "\u252c",
    header_end: "\u2510",

    row_beginning: "\u2502",
    row_crossing: "\u2502",
    row_end: "\u2502",

    row_separator_beginning: "\u251c",
    row_separator_crossing: "\u253c",
    row_separator_end: "\u2524",

    footer_beginning: "\u2514",
    footer_crossing: "\u2534",
    footer_end: "\u2518",

    horizontal_line: "\u2500",
  }.freeze

  attr_reader :rows,
              :row_labels,
              :column_labels

  def initialize(
    rows:,
    row_labels: [],
    column_labels: [],
    drawing_characters: UNICODE_BOX_CHARACTERS,
    formatter: ->(value) { "\s#{value}\s" },
    io: STDOUT
  )
    @rows = rows
    @row_labels = row_labels
    @column_labels = column_labels
    @drawing_characters = drawing_characters
    @formatter = formatter
    @io = io
  end

  def print
    rows = add_row_labels(@rows)
    rows = add_column_labels(rows)

    rows, column_widths = transform_values_to_strings(rows)

    rows.each_with_index do |row, i|
      if i.zero?
        print_header(column_widths)
      else
        print_row_separator(column_widths)
      end

      print_row(row, column_widths)
    end

    print_footer(column_widths)
  end

  private

  def add_row_labels(rows)
    unless @row_labels.empty?
      rows.zip(@row_labels).map do |row, label|
        [label] + row
      end
    else
      rows
    end
  end

  def add_column_labels(rows)
    unless @column_labels.empty?
      unless @row_labels.empty?
        # if there are row labels and one empty cell
        # in the top left to aling correctly the column labels
        [[''] + @column_labels] + rows
      else
        [@column_labels] + rows
      end
    else
      rows
    end
  end

  def transform_values_to_strings(rows)
    column_widths = []

    rows = rows.map do |row|
      row.each_with_index.map do |value, i|
        value = @formatter ? @formatter.call(value) : value.to_s

        column_widths[i] = [value.size, column_widths[i] || 0].max

        value
      end
    end

    [rows, column_widths]
  end

  def print_header(column_widths)
    print_line(
      column_widths,
      @drawing_characters[:header_beginning],
      @drawing_characters[:header_crossing],
      @drawing_characters[:header_end]
    )
  end

  def print_row_separator(column_widths)
    print_line(
      column_widths,
      @drawing_characters[:row_separator_beginning],
      @drawing_characters[:row_separator_crossing],
      @drawing_characters[:row_separator_end]
    )
  end

  def print_footer(column_widths)
    print_line(
      column_widths,
      @drawing_characters[:footer_beginning],
      @drawing_characters[:footer_crossing],
      @drawing_characters[:footer_end]
    )
  end

  def print_line(
    column_widths,
    beginning_character,
    crossing_character,
    end_character,
    horizontal_line = @drawing_characters[:horizontal_line]
  )
    column_widths.each_with_index do |width, i|
      if i.zero?
        @io.write(beginning_character)
      else
        @io.write(crossing_character)
      end

      @io.write(horizontal_line * width)
    end

    @io.write(end_character)
    @io.write("\n")
  end

  def print_row(
    row,
    column_widths,
    beginning_character = @drawing_characters[:row_beginning],
    crossing_character = @drawing_characters[:row_crossing],
    end_character = @drawing_characters[:row_end]
  )
    row.each_with_index do |value, i|
      if i.zero?
        @io.write(beginning_character)
      else
        @io.write(crossing_character)
      end

      @io.write('%0*s' % [column_widths[i], value])
    end

    @io.write(end_character)
    @io.write("\n")
  end
end

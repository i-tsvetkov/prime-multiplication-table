class PrimeGenerator
  include Enumerable

  # https://en.wikipedia.org/wiki/Primality_test#Simple_methods
  Is = [1, 7, 11, 13, 17, 19, 23, 29]

  def initialize
    @primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
  end

  def each
    @primes.each{ |prime| yield prime }

    (1..).each do |k|
      Is.each do |i|
        n = 30 * k + i

        if prime?(n)
          yield n

          @primes << n
        end
      end
    end
  end

  private

  def prime?(n)
    limit = Math.sqrt(n)

    @primes.each do |prime|
      break if prime > limit

      return false if n.modulo(prime).zero?
    end

    true
  end
end
